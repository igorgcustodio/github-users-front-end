# GitHub Users API - Front-end

This project is intended to manipulate the API responses of [GitHub Users API](https://github.com/igorgcustodio/github-users-back-end).

The main purpose is to practice and learning React

## Dependencies

### Development

- Axios @ 0.15.3
- Babel-core @ 6.22.1
- Babel-loader @ 6.2.10
- Babel-plugin-react-html-attrs @ 2.0.0
- Babel-plugin-transform-object-rest-spread @ 6.22.0
- Babel-preset-es2015 @ 6.22.0
- Babel-preset-react @ 6.22.0
- Bootstrap @ 3.3.7
- Css-loader @ 0.26.1
- Extract-text-webpack-plugin @ 1.0.1
- File-loader @ 0.9.0
- Font-awesome @ 4.7.0
- React @ 15.4.2
- React-dom @ 15.4.2
- React-router @ 3.0.2
- Style-loader @ 0.13.1
- Webpack @ 1.14.0
- Webpack-dev-serv @ 1.16.2
