import React from 'react'
import { Router, Route, Redirect, hashHistory} from 'react-router'

import Profile from '../user/profile'
import UsersList from '../user/users-list'

export default props => (
	<Router history={hashHistory}>
		<Route path='/users' component={UsersList} />
		<Route path='/users/:username' component={Profile} />
		<Redirect from='*' to='/users'/>
	</Router>
)