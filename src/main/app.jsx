import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'
import '../assets/style.css'

import React from 'react'
import Header from '../template/header'
import Routes from '../config/routes'

export default props => (
	<div className='container-fluid'>
		<Header />
		<Routes />
	</div>
)