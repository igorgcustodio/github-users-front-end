import React from 'react'

export default props => (
	<i className={'fa fa-' + props.icon} aria-hidden="true"></i>
)