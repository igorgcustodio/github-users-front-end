import React, { Component } from 'react'
import Icon from '../template/icon'

export default class Details extends Component {
	
	render() {
		return (
			<div className="col-xs-12">
				<div className="col-xs-12 col-sm-3">
					<img src="https://avatars0.githubusercontent.com/u/10201920?v=4" alt="Igor Guilherme Custodio" className="img-responsive image-profile"/>
				</div>
				<div className="col-xs-12 col-sm-9">
					<h1>Igor Guilherme Custodio <small><a href="https://github.com/igorgcustodio" target='_blank'>igorgcustodio</a></small></h1>
					<p>User ID: 10201920</p>
					<p>Computer Engineering student, web developer and IoT enthusiast 🎱 </p>
					<p>Company name - Location</p>
					<p><Icon icon='users' /> 10 followers </p>
					<p><Icon icon='user-plus' /> 10 following </p>
					<p><Icon icon='envelope' /> igor.custodio8@gmail.com </p>
				</div>
			</div>
		)
	}
}