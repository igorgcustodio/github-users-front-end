import React, { Component } from 'react'
import Axios from 'axios'
import config from '../config/config'

import Details from './details'
import Repos from './repos'

export default class Profile extends Component {

	constructor() {
		super()
		this.user = null
	}

	getUserData(username) {
		Axios.get(config.BASE_URL + '/' + username + '/details')
			.then(function(response) {
				console.log(1)
			})
	}

	render() {
		return (
			<section>
				<div className="row">
					<Details />
				</div>
				<div className="row">
					<hr/>
				</div>
				<div className="row">
					<Repos />
				</div>
			</section>
		)
	}
}