import React, { Component } from 'react'
import config from '../config/config'
import Axios from 'axios'

export default class Repos extends Component {

	constructor() {
		super()
	}

	getReposByUser(username) {
		Axios.get(config.BASE_URL + '/' + username + '/repos')
			.then(function(response) {
				console.log(1)
			})
	}

	render() {
		return (
			<div className="col-xs-12">
				<table className="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Language</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td><a href="">repositorio-top</a></td>
							<td>C</td>
						</tr>
					</tbody>
				</table>
			</div>
		)
	}
}