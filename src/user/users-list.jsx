import React, { Component } from 'react'

export default class UsersList extends Component {
	render() {
		return (
			<section>
				<div className="row">
					<table className="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Login</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Igor Guilherme Custodio</td>
								<td><a href={'#/users/'+'igorgcustodio'}>igorgcustodio</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</section>
		)
	}
}